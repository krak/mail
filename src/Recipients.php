<?php

namespace Krak\Mail;

class Recipients {
    public $to = [];
    public $cc = [];
    public $bcc = [];
    public $from = [];
}
