<?php

namespace Krak\Mail;

use Swift_Mailer,
    Swift_Message;

function swift_html_mailer(Swift_Mailer $mailer) {
    return function($subject, Recipients $recipients, $content) use ($mailer) {
        $msg = new Swift_Message($subject);
        $msg->setFrom($recipients->from);
        $msg->setTo($recipients->to);
        $msg->setCc($recipients->cc);
        $msg->setBcc($recipients->bcc);
        $msg->addPart($content, 'text/html');

        $mailer->send($msg);
    };
}

function echo_mailer() {
    return function($subject, Recipients $recipients, $content) {
        echo $content;
    };
}
