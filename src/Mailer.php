<?php

namespace Krak\Mail;

interface Mailer
{
    public function __invoke($subject, Recipients $recipients, $content);
}
