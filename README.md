# Mail

The mail library is a simple library for abstracting and simplifying sending emails.

## Usage

```php
<?php

$mailer = Krak\Mail\swift_html_mailer($swift_mailer);
$prefix_mailer = function($subject Krak\Mail\Recipients $recipients, $content) use ($mailer) {
    return $mailer('prefix - ' . $subject, $recipients, $content);
};

$r = new Krak\Mail\Recipients();
$r->to = ['rj@bighead.net' => 'RJ Garcia'];
$prefix_mailer('Sending Email!', $r, '<h1>Header!</h1>');
```

For debugging purposes, there is also the `echo_mailer`
